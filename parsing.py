import requests
from bs4 import BeautifulSoup
import pandas as pd

def parse_url(req): # получение ссылок с сайта sports.ru
    bsObj=BeautifulSoup(req.text,'lxml') 
    links = []
    for i in bsObj.find_all('a', attrs = {'class':"name"}):
        links.append(i.get('href'))
    print("Ссылки на команды АПЛ с сайта sports.ru:")
    print(pd.Series(links))
    return links

url = 'https://www.sports.ru/epl/table/'

req = requests.get(url) 

print(req.status_code)

if req.status_code==200:
    print("Соединение с сайтом установлено")
    links = parse_url(req)
else:
    print("Соединение с сайтом не установлено")
  
